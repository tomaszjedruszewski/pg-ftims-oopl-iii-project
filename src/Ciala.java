package cwiczenie;

public class Ciala implements Funkcje
{
	private String nazwa;
	private int srednica;
	private int masa;
	
	public Ciala()
	{
		this("Brak nazwy", 0, 0);
	}
	
	public Ciala(String nazwa, int srednica, int masa)
	{
		this.setNazwa(nazwa);
		this.setSrednica(srednica);
		this.setMasa(masa);
	}

	public String getNazwa() 
	{
		return nazwa;
	}

	public void setNazwa(String nazwa) 
	{
		this.nazwa = nazwa;
	}

	public int getSrednica() 
	{
		return srednica;
	}

	public void setSrednica(int srednica) 
	{
		this.srednica = srednica;
	}

	public int getMasa() 
	{
		return masa;
	}

	public void setMasa(int masa) 
	{
		this.masa = masa;
	}
	
	public String toString()
	{
		return "Nazwa: "+this.nazwa+", �rednica: "+this.srednica+", masa: "+this.masa;
	}
	
	@Override
	public void brak() {
		System.out.println("Takie cia�o niebieskie nie istnieje.");
		
	}

	@Override
	public int promien() {
		return getSrednica()/2;
	}
}