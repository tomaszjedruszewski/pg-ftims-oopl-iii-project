package cwiczenie;

public class Gwiazda extends Ciala implements Nasze
{
	private String gwiazdozbior;
	private String typ;
	private int wiek;
	
	public Gwiazda()
	{
		this("Brak gwiazdozbioru", "Brak typu układu", 0);
	}
	
	public Gwiazda (String gwiazdozbior, String typ, int wiek)
	{
		this.setGwiazdozbior(gwiazdozbior);
		this.setTyp(typ);
		this.setWiek(wiek);
	}

	public String getGwiazdozbior() 
	{
		return gwiazdozbior;
	}

	public void setGwiazdozbior(String gwiazdozbior) 
	{
		this.gwiazdozbior = gwiazdozbior;
	}

	public String getTyp() 
	{
		return typ;
	}

	public void setTyp(String typ) 
	{
		this.typ = typ;
	}

	public int getWiek() 
	{
		return wiek;
	}

	public void setWiek(int wiek) 
	{
		this.wiek = wiek;
	}
	
	@Override
	public void slonce() {
		System.out.println("Słońce to nasza najbliższa gwiazda!");
		
	}

	
}
