package cwiczenie;

public class Naturalny extends Satelita
{	
	private int rok;
	public Naturalny()
	{
		this(0);
	}
	
	public Naturalny(int rok)
	{
		this.setRok(rok);
	}

	public int getRok() 
	{
		return rok;
	}

	public void setRok(int rok) 
	{
		this.rok = rok;
	}
}
