package cwiczenie;

public class Planeta extends Ciala 
{
	private String gwiazda;
	private int ksiezyc;
	private int r_orbity;
	private int okres_obiegu;
	
	public Planeta()
	{
		this("nie zdeklarowano",0,0,0);
	}
	
	public Planeta(String gwiazda, int ksiezyc, int r_orbity, int okres_obiegu)
	{
		this.setGwiazda(gwiazda);
		this.setKsiezyc(ksiezyc);
		this.setR_orbity(r_orbity);
		this.setOkres_obiegu(okres_obiegu);
	}

	public String getGwiazda() 
	{
		return gwiazda;
	}

	public void setGwiazda(String gwiazda) 
	{
		this.gwiazda = gwiazda;
	}

	public int getKsiezyc() 
	{
		return ksiezyc;
	}

	public void setKsiezyc(int ksiezyc) 
	{
		this.ksiezyc = ksiezyc;
	}

	public int getR_orbity() 
	{
		return r_orbity;
	}

	public void setR_orbity(int r_orbity) 
	{
		this.r_orbity = r_orbity;
	}

	public int getOkres_obiegu() 
	{
		return okres_obiegu;
	}

	public void setOkres_obiegu(int okres_obiegu) 
	{
		this.okres_obiegu = okres_obiegu;
	}
	
}
