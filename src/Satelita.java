package cwiczenie;

public class Satelita extends Ciala
{
	private String co_obiega;
	
	public Satelita()
	{
		this("nie zdeklarowano");
	}
	
	public Satelita(String co_obiega)
	{
		this.setCo_obiega(co_obiega);
	}

	public String getCo_obiega() 
	{
		return co_obiega;
	}

	public void setCo_obiega(String co_obiega) 
	{
		this.co_obiega = co_obiega;
	}
}
