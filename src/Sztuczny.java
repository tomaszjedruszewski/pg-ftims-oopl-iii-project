package cwiczenie;

public class Sztuczny extends Satelita
{
	private String kraj;
	private String cel;
	
	public Sztuczny()
	{
		this("nie wiem", "brak wiedzy");
	}
	
	public Sztuczny(String kraj, String cel)
	{
		this.setKraj(kraj);
		this.setCel(cel);
	}

	public String getKraj() 
	{
		return kraj;
	}

	public void setKraj(String kraj) 
	{
		this.kraj = kraj;
	}

	public String getCel() 
	{
		return cel;
	}

	public void setCel(String cel) 
	{
		this.cel = cel;
	}
}

