package cwiczenie;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.*;

public class Test{
	public static void main (String [] args) throws IOException
	{
		List<String> listaGwiazda = new ArrayList<String>();
		List<String> listaPlaneta = new ArrayList<String>();
		List<String> listaKsiezyc = new ArrayList<String>();
		List<String> listaSatelita = new ArrayList<String>();
		
		System.out.println("Podaj nazwy gwiazd:");
		BufferedReader bufferGwiazda = new BufferedReader(new InputStreamReader(System.in));
		String gwiazda;
		while ((gwiazda = bufferGwiazda.readLine()) != null  && !gwiazda.endsWith("."))
		{ 
			listaGwiazda.add(gwiazda);
		}
		listaGwiazda.add(gwiazda.substring(0,gwiazda.length()-1));
		
		System.out.println("Podaj nazwy planet:");
		BufferedReader bufferPlaneta = new BufferedReader(new InputStreamReader(System.in));
		String planeta;
		while ((planeta = bufferPlaneta.readLine()) != null  && !planeta.endsWith("."))
		{
			  listaPlaneta.add(planeta);
		}
		listaPlaneta.add(planeta.substring(0, planeta.length()-1));
		
		System.out.println("Podaj nazwy ksiezycow:");
		BufferedReader bufferKsiezyc = new BufferedReader(new InputStreamReader(System.in));
		String ksiezyc;
		while ((ksiezyc = bufferKsiezyc.readLine()) != null  && !ksiezyc.endsWith("."))
		{
			  listaKsiezyc.add(ksiezyc);
		}
		listaKsiezyc.add(ksiezyc.substring(0, ksiezyc.length()-1));
		
		System.out.println("Podaj nazwy satelit:");
		BufferedReader bufferSatelita = new BufferedReader(new InputStreamReader(System.in));
		String satelita;
		while ((satelita = bufferSatelita.readLine()) != null  && !satelita.endsWith("."))
		{
			  listaSatelita.add(satelita);
		}
		listaSatelita.add(satelita.substring(0, satelita.length()-1));
		
		/*
		for(int i=0; i < listaGwiazda.size(); i++)
		{
			System.out.println(listaGwiazda.get(i));
		}
		for(int i=0; i < listaPlaneta.size(); i++)
		{
			System.out.println(listaPlaneta.get(i));
		}
		for(int i=0; i < listaKsiezyc.size(); i++)
		{
			System.out.println(listaKsiezyc.get(i));
		}
		for(int i=0; i < listaSatelita.size(); i++)
		{
			System.out.println(listaSatelita.get(i));
		}
		System.out.println("To wszystkie ciala niebieskie.");*/
		
		Map<String, List<String>> mapaGwiazda = new HashMap<String, List<String>>();
		Map<String, List<String>> mapaPlaneta = new HashMap<String, List<String>>();
		Map<String, List<String>> mapaKsiezyc = new HashMap<String, List<String>>();
		Map<String, List<String>> mapaSatelita = new HashMap<String, List<String>>();
		List<String> lista = new ArrayList<String>();
		
		for(int i=0 ; i<listaGwiazda.size(); i++)
		{
			System.out.println("Podaj szcze��we dane "+listaGwiazda.get(0)+" w formie:");
			System.out.println("�rednica");
			System.out.println("masa");
			System.out.println("gwiazdozbi�r");
			System.out.println("typ uk�adu");
			System.out.println("wiek gwiazdy w mld lat.");
			
			BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
			String wers;
			lista.clear();
			while ((wers = buffer.readLine()) != null  && !wers.endsWith("."))
			{
				  lista.add(wers);
			}
			lista.add(wers.substring(0, wers.length()-1));	
			
			mapaGwiazda.put(listaGwiazda.get(i), lista);
			
			for(Map.Entry<String, List<String>> entry : mapaGwiazda.entrySet())
			{
				String key = entry.getKey();
				List<String> values = entry.getValue();
				
				System.out.println("Klucz = " + key);
				System.out.println("Warto�ci = " + values + "\n");
				
			}
		}
		
		for(int i=0 ; i<listaPlaneta.size(); i++)
		{
			System.out.println("Podaj szcze��we dane "+listaPlaneta.get(0)+" w formie:");
			System.out.println("�rednica");
			System.out.println("masa");
			System.out.println("uk�ad s�oneczny");
			System.out.println("ilo�� ksi�ycy");
			System.out.println("promie� orbity");
			System.out.println("okres orbitalny.");
			
			BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
			String wers;
			lista.clear();
			while ((wers = buffer.readLine()) != null  && !wers.endsWith("."))
			{
				  lista.add(wers);
			}
			lista.add(wers.substring(0, wers.length()-1));	
			
			mapaPlaneta.put(listaPlaneta.get(i), lista);
			
			for(Map.Entry<String, List<String>> entry : mapaPlaneta.entrySet())
			{
				String key = entry.getKey();
				List<String> values = entry.getValue();
				
				System.out.println("Klucz = " + key);
				System.out.println("Warto�ci = " + values + "\n");
				
			}
		}
		
		for(int i=0 ; i<listaKsiezyc.size(); i++)
		{
			System.out.println("Podaj szcze��we dane "+listaKsiezyc.get(0)+" w formie:");
			System.out.println("�rednica");
			System.out.println("masa");
			System.out.println("co obiega");
			System.out.println("rok odkrycia.");
			
			BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
			String wers;
			lista.clear();
			while ((wers = buffer.readLine()) != null  && !wers.endsWith("."))
			{
				  lista.add(wers);
			}
			lista.add(wers.substring(0, wers.length()-1));	
			
			mapaKsiezyc.put(listaKsiezyc.get(i), lista);
			
			for(Map.Entry<String, List<String>> entry : mapaKsiezyc.entrySet())
			{
				String key = entry.getKey();
				List<String> values = entry.getValue();
				
				System.out.println("Klucz = " + key);
				System.out.println("Warto�ci = " + values + "\n");
				
			}
		}
		
		for(int i=0 ; i<listaSatelita.size(); i++)
		{
			System.out.println("Podaj szcze��we dane "+listaSatelita.get(0)+" w formie:");
			System.out.println("�rednica");
			System.out.println("masa");
			System.out.println("co obiega");
			System.out.println("kraj pochodzenia");
			System.out.println("cel misji.");
			
			BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
			String wers;
			lista.clear();
			while ((wers = buffer.readLine()) != null  && !wers.endsWith("."))
			{
				  lista.add(wers);
			}
			lista.add(wers.substring(0, wers.length()-1));	
			
			mapaSatelita.put(listaSatelita.get(i), lista);
			
			for(Map.Entry<String, List<String>> entry : mapaSatelita.entrySet())
			{
				String key = entry.getKey();
				List<String> values = entry.getValue();
				
				System.out.println("Klucz = " + key);
				System.out.println("Warto�ci = " + values + "\n");
				
			}
		}
		
			Ciala star = new Gwiazda();
	}


	

}
